# README #

### What is this readme for? ###

* Quick summary
* Version
* [Sample Config](https://bitbucket.org/azunai/sesmarteconomy/src/master/SampleConfig.xml)
* Additional information


### Who do I talk to? ###

* Azunai


### Commands: ###
* `!help SESE`  
Shows all commands of SESE.

* `!SESE Reload`  
Reloads the Plugin Config. Restarts the Timer for RefreshRate.

* `!SESE RefreshStores`  
Triggers a refresh cylce on all SmartStores.

* `!SESE AllowStore <ulong steam64id> <string guid>`  
Adds a player to a Store of type `<guid>`

* `!SESE DenyStore <ulong steam64id> <string guid>`  
Removes a player from a Store of type `<guid>`

* `!SESE AllowStoreFaction <string factionTag> <string guid>`  
Adds a faction to a Store of type `<guid>`

* `!SESE DenieStoreFaction <string factionTag> <string guid>`  
Removes a faction from a Store of type `<guid>`

* `!SESE ShowStores`  
Shows a list of all Storetypes.

* `!SESE DumpDefinitions`  
Creates a file "SESEDefinitions.txt" in the torch folder containing all available definitions. Not all work in the store!

### Config: ###
* `<Stores></Stores>`  
Is a list for all your SESmartEconomy Stores.

* `<SESmartEconomyStore>`  
This is one SESmartEconomy Store.

* `<GUID>MyFirstStore</GUID>`  
String  
GUID is a unique string. All ingame Stores with that String in CustomData will be converted to SESmartEconomy Stores. It can be as complex and long as CustomData accepts it.

* `<RefreshRate>60</RefreshRate>`  
unit  
Time in seconds between StoreItem Listings refresh.

* `<SESEStoreItems>`  
Is a list for all your StoreItems.

* `<SESmartEconomyStoreItem>`  
This is one StoreItem.

* `<ItemDefinitionType>MyObjectBuilder_Ore</ItemDefinitionType>`  
String  
This defines the ingame ItemType.

* `<ItemDefinitionSubType>Gold</ItemDefinitionSubType>`  
String  
This defines the ingame ItemSubType.

* `<Amount>1337</Amount>`  
int  
This is the item’s amount per sell/buy order.

* `<BasePrice>7331</BasePrice>`  
int  
This defines the base price for the given item.  

* `<ListingType>Offer</ListingType>`  
Enum - Offer,Order  
This defines if the item should be listed as an `Offer` or `Order`.

* `<DisplayChance>100</DisplayChance>`  
float Value 0 - 100  
This is the Chance for the given item to be visible in the Store.

* `<MarketMultiplier>1</MarketMultiplier>`  
float  
This is the ratio of how much the item’s price should scale depending on players buying or selling the item. 
Baseprice multiplier, if SmartPriceEnabled = True this is handled by the Plugin

* `<MarketMultiplierUpperLimit>2</MarketMultiplierUpperLimit>`  
float  
Upper multiplier limit to avoid market collapse or infinite price growth

* `<MarketMultiplierLowerLimit>0</MarketMultiplierLowerLimit>`  
float  
Lower multiplier limit to avoid market collapse or 1 Credit prices

* `<MarketMultiplierListingIgnoreStep>0.025</MarketMultiplierListingIgnoreStep>`  
float  
Multiplier adjustment if Listing is ignored, is applied every listing refresh

* `<MarketMultiplierListingFillStep>0.1</MarketMultiplierListingIgnoreStep>`  
float  
Multiplier adjustment if Listing is filled to at least 75%, is applied every listing refresh

* `<SmartPriceEnabled>true</SmartPriceEnabled>`  
bool  
Enables dynamic pricing

* `<SoldSinceLastUpdate>0</SoldSinceLastUpdate>`  
int  
Handled by plugin

* `<BoughtSinceLastUpdate>0</BoughtSinceLastUpdate>`  
int  
Handled by plugin

* `<AllowedUser>`  
This is a list of Users who can access the Store.

* `<unsignedLong>76501239044075</unsignedLong>`  
This is the Steam64ID of an allowed player.

* `<AllowedFactions>`  
This is a list of Users who can access the Store.

* `<unsignedLong>14576501239044075</unsignedLong>`  
This is the unique faction ID of an allowed Faction.

* `<Enabled>true</Enabled>`  
bool  
This enables (true) or disables(false) the Plugin.

* `<Statistics>true</Statistics>`  
bool  
This enables logging for each sold or bought item.

* `<StatisticsLog>Log/SESEStatistics.log</StatisticsLog>`  
String  
This sets the destination file & folder for the statistics log.

### [Sample Config](https://bitbucket.org/azunai/sesmarteconomy/src/master/SampleConfig.xml) ###

### Additional Information ###
* I higly recommand to create your stores directly in the config file. The available commands are mostly for debugging or managing permissions live.
* Turning a Store Off and On will cause a Refresh -> If SmartPrices are enabled, prices will change.
 You can use this behaviour for special cases and simply script events ingame with Sensors or Programmable Blocks!
* If you do not use AllowedUser or AllowedFactions, it will default to allowing everyone!
* Stores use Economy Balance of Store owner, so make sure you assign an NPC. If you want to drain Credits out of the game, dont give them Credits on start, so players have to first buy items before the store is able to buy from Players.
* SmartPrice behaviour is currently hardcoded: If a shop sells more than 75% of an item, its price will increase by defined MarketMultiplierListingFillStep
If the item is not sold at all, it will decrease by defined MarketMultiplierListingIgnoreStep.
On the contrary, if a shops order is filled by at least 75%, the prices on that order will decrease. And if not a single item is sold to the shop, prices will increase.
Make sure to use Upper and Lower limits to avoid inflation.
