﻿using Sandbox;
using Sandbox.Game;
using Sandbox.Game.Entities;
using Sandbox.Game.Entities.Blocks;
using Sandbox.Game.Multiplayer;
using Sandbox.Game.World;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Timers;
using Torch.Managers.PatchManager;
using VRage;
using VRage.Game;
using VRage.Game.Entity;
using VRage.Game.ModAPI;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Network;
using VRage.ObjectBuilders;
using VRage.Sync;


namespace SESmartEconomy
{
    [PatchShim]
    public static class MyStoreBlockPatch
    {
        //internal static readonly MethodInfo Method_Init = typeof(MyStoreBlock).GetMethod("Init", new[] { typeof(MyObjectBuilder_CubeBlock), typeof(MyCubeGrid) }) ?? throw new Exception("Failed to find MyStoreBlock.Init method");
        //internal static readonly MethodInfo Method_InitPatched = typeof(MyStoreBlockPatch).GetMethod("InitPatched", BindingFlags.Static | BindingFlags.Public) ?? throw new Exception("Failed to find MyStoreBlockPatch.InitPatched method");

        internal static readonly MethodInfo Method_RefreshVolumeAndMass = typeof(MyInventory).GetMethod("RefreshVolumeAndMass", BindingFlags.Instance | BindingFlags.NonPublic) ?? throw new Exception("Failed to find MyInventory.RefreshVolumeAndMass method");
        internal static readonly MethodInfo Method_RefreshVolumeAndMassPatched = typeof(MyStoreBlockPatch).GetMethod("RefreshVolumeAndMassPatched", BindingFlags.Static | BindingFlags.Public) ?? throw new Exception("Failed to find MyStoreBlockPatch.RefreshVolumeAndMassPatched method");

        internal static readonly MethodInfo Method_GetStoreItems = typeof(MyStoreBlock).GetMethod("GetStoreItems", BindingFlags.Instance | BindingFlags.NonPublic) ?? throw new Exception("Failed to find MyStoreBlock.GetStoreItems method");
        internal static readonly MethodInfo Method_GetStoreItemsPatched = typeof(MyStoreBlockPatch).GetMethod("GetStoreItemsPatched", BindingFlags.Static | BindingFlags.Public) ?? throw new Exception("Failed to find MyStoreBlockPatch.GetStoreItemsPatched method");

        internal static readonly MethodInfo Method_OnStartWorking = typeof(MyStoreBlock).GetMethod("OnStartWorking", BindingFlags.Instance | BindingFlags.NonPublic) ?? throw new Exception("Failed to find MyStoreBlock.OnStartWorking method");
        internal static readonly MethodInfo Method_OnStartWorkingPatched = typeof(MyStoreBlockPatch).GetMethod("OnStartWorkingPatched", BindingFlags.Static | BindingFlags.Public) ?? throw new Exception("Failed to find MyStoreBlockPatch.OnStartWorkingtPatched method");

        internal static readonly MethodInfo Method_SendSellItemResult = typeof(MyStoreBlock).GetMethod("SendSellItemResult", BindingFlags.Instance | BindingFlags.NonPublic, null, new Type[] { typeof(long), typeof(string), typeof(long), typeof(int), typeof(MyStoreSellItemResults) }, null) ?? throw new Exception("Failed to find MyStoreBlock.SendSellItemResult method");
        internal static readonly MethodInfo Method_SendSellItemResultPatched = typeof(MyStoreBlockPatch).GetMethod("SendSellItemResultPatched", BindingFlags.Static | BindingFlags.Public) ?? throw new Exception("Failed to find MyStoreBlockPatch.SendSellItemResultPatched method");

        internal static readonly MethodInfo Method_SendBuyItemResult = typeof(MyStoreBlock).GetMethod("SendBuyItemResult", BindingFlags.Instance | BindingFlags.NonPublic, null, new Type[] { typeof(long), typeof(string), typeof(long), typeof(int), typeof(MyStoreBuyItemResults), typeof(EndpointId) }, null) ?? throw new Exception("Failed to find MyStoreBlock.SendBuyItemResult method");
        internal static readonly MethodInfo Method_SendBuyItemResultPatched = typeof(MyStoreBlockPatch).GetMethod("SendBuyItemResultPatched", BindingFlags.Static | BindingFlags.Public) ?? throw new Exception("Failed to find MyStoreBlockPatch.SendBuyItemResultPatched method");

        internal static readonly MethodInfo Method_OnRemovedByCubeBuilder = typeof(MyStoreBlock).GetMethod("OnRemovedByCubeBuilder", BindingFlags.Instance | BindingFlags.Public) ?? throw new Exception("Failed to find MyStoreBlock.OnRemovedByCubeBuilder method");
        internal static readonly MethodInfo Method_OnRemovedByCubeBuilderPatched = typeof(MyStoreBlockPatch).GetMethod("OnRemovedByCubeBuilderPatched", BindingFlags.Static | BindingFlags.Public) ?? throw new Exception("Failed to find MyStoreBlockPatch.OnRemovedByCubeBuilder method");

        internal static readonly MethodInfo Method_OnItemBought = typeof(MyStoreBlock).GetMethod("OnItemBought", BindingFlags.Instance | BindingFlags.NonPublic) ?? throw new Exception("Failed to find MyStoreBlockOnItemBought method");
        internal static readonly MethodInfo Method_OnItemBoughtPatched = typeof(MyStoreBlockPatch).GetMethod("OnItemBoughtPatched", BindingFlags.Static | BindingFlags.Public) ?? throw new Exception("Failed to find MyStoreBlockPatch.OnItemBought method");

 

        internal static FieldInfo Field_m_maxVolume = typeof(MyInventory).GetField("m_maxVolume", BindingFlags.Instance | BindingFlags.NonPublic);
        internal static FieldInfo Field_m_currentMass = typeof(MyInventory).GetField("m_currentMass", BindingFlags.Instance | BindingFlags.NonPublic);
        internal static FieldInfo Field_m_currentVolume = typeof(MyInventory).GetField("m_currentVolume", BindingFlags.Instance | BindingFlags.NonPublic);

        public static IDictionary<long, SESEStoreTimer> timerDict = new Dictionary<long, SESEStoreTimer>();
        public static IDictionary<string, List<MyStoreItem>> storeItemsDict = new Dictionary<string, List<MyStoreItem>>();
        public static IDictionary<long, ulong> startWorkingBlacklistDict = new Dictionary<long, ulong>();

        public static event Action<MyStoreBlock, long, string, long, int, MyStoreSellItemResults> OnSendSellItemResult;
        public static event Action<MyStoreBlock, long, string, long, int, MyStoreBuyItemResults, EndpointId> OnSendBuytemResult;
        public static event Action<MyStoreBlock, MyInventory, MyDefinitionId, long, int> OnItemBought;

        

        public static void OnItemBoughtPatched(MyStoreBlock __instance, MyInventory inventory, MyDefinitionId definitionId, long totalPrice, int amount)
        {
            SESmartEconomy.Log.Trace("OnItemBoughtPatched");
            if (!SESmartEconomy.Instance.Config.Enabled)
                return;
            if (__instance is MyStoreBlock && __instance != null)
            {
                SESmartEconomyStore store = GetSESEStore(__instance.CustomData);
                if (store != null)
                {
                    OnItemBought?.Invoke(__instance, inventory, definitionId, totalPrice, amount);
                }
            }
        }

        public static void ReloadTimer()
        {
            SESmartEconomy.Log.Trace("ReloadTimer");
            foreach (KeyValuePair<long, SESEStoreTimer> item in timerDict)
            {
                MyStoreBlock myStore = MyEntities.GetEntityById(item.Key) as MyStoreBlock;
                if (myStore != null)
                {
                    SESmartEconomyStore store = GetSESEStore(myStore.CustomData);
                    if (store != null)
                    {
                        item.Value.Interval = store.RefreshRate * 1000;
                        //SESmartEconomy.Log.Info("new rate: " + store.RefreshRate);       
                    }
                }
            }
        }

        public static void RefreshStores()
        {
            foreach (KeyValuePair<long, SESEStoreTimer> item in timerDict)
            {               
                MyStoreBlock myStore = MyEntities.GetEntityById(item.Key) as MyStoreBlock;
                if (myStore != null)
                {
                    
                    SESmartEconomyStore store = GetSESEStore(myStore.CustomData);
                    if (store != null)
                    {                       
                        //Method_OnStartWorking.Invoke(myStore, new object[] { });     
                        OnStartWorkingPatched(myStore);
                    }
                }
            }
        }
        public static void ReloadStores()
        {
            storeItemsDict.Clear();            
        }
        public static void OnRemovedByCubeBuilderPatched(MyStoreBlock __instance)
        {
            SESmartEconomy.Log.Trace("OnRemovedByCubeBuilderPatched");
            if (!SESmartEconomy.Instance.Config.Enabled)
                return;
            if (__instance is MyStoreBlock && __instance != null)
            {
                SESmartEconomyStore store = GetSESEStore(__instance.CustomData);
                if (store != null)
                {
                    SESmartEconomy.Log.Debug("Store removed by CubeBuilder - clearing inventory");
                    __instance.GetInventory().Clear();
                }
            }
        }
        public static void Closing(MyEntity entity)
        {
            SESmartEconomy.Log.Trace("Closing: " + entity.EntityId);
            if (entity is MyStoreBlock && entity != null)
            {
                //SESmartEconomy.Log.Info("Closing");               
                if (timerDict.TryGetValue(entity.EntityId, out SESEStoreTimer timer))
                {
                    //this actualy disposes the timer cleanly
                    timerDict.Remove(entity.EntityId);
                    timer.Stop();
                }
            }
        }
        public static void SendSellItemResultPatched(MyStoreBlock __instance, long id, string name, long price, int amount, MyStoreSellItemResults result)
        {
            SESmartEconomy.Log.Trace("SendSellItemResultPatched");
            if (!SESmartEconomy.Instance.Config.Enabled)
                return;
            SESmartEconomyStore store = GetSESEStore(__instance.CustomData);
            if (store != null)
            {
                //SESmartEconomy.Log.Info("MyStoreBlock SendSellItemResultPatched");
                if (__instance is MyStoreBlock)
                {
                    if (result == MyStoreSellItemResults.Success)
                    {
                        //SESmartEconomy.Log.Info("MyStoreBlock SendSellItemResultPatched success " + name + " " + price + " " + amount + " " + id);
                        foreach (var item in store.SESEStoreItems)
                        {
                            //if (item.ItemDefinitionId.SubtypeName.Equals(name))
                            if(item.StoreItemId == id)
                            {
                                //SESmartEconomy.Log.Info("item sold");
                                item.SoldSinceLastUpdate += amount;
                                if (SESmartEconomy.Instance.Config.Statistics)
                                    SESmartEconomy.LogStatistic.Info(",GUID:," + __instance.CustomData + ",Sold,Amount:," + amount + ",Item:," + name + ",Price:," + price + ",Player:," + MyEventContext.Current.Sender);
                                SESmartEconomy.Log.Debug("id: " + id + ", GUID: " + __instance.CustomData + ", Sold, Amount: " + amount + ", Item: " + name + ", Price: " + price + ", Player: " + MyEventContext.Current.Sender);

                                OnSendSellItemResult?.Invoke(__instance, id, name, price, amount, result);
                                return;
                            }
                        }
                    }

                }
            }
        }

        public static void SendBuyItemResultPatched(MyStoreBlock __instance, long id, string name, long price, int amount, MyStoreBuyItemResults result, EndpointId targetEndpoint)
        {
            SESmartEconomy.Log.Trace("SendBuyItemResultPatched");
            if (!SESmartEconomy.Instance.Config.Enabled)
                return;
            SESmartEconomyStore store = GetSESEStore(__instance.CustomData);
            if (store != null)
            {
                //SESmartEconomy.Log.Info("MyStoreBlock SendBuyItemResultPatched");
                if (__instance is MyStoreBlock)
                {
                    if (result == MyStoreBuyItemResults.Success)
                    {
                        //SESmartEconomy.Log.Info("MyStoreBlock SendSellItemResultPatched success " + name + " " + price + " " + amount + " " + id);
                        foreach (var item in store.SESEStoreItems)
                        {
                            //if (item.ItemDefinitionId.SubtypeName.Equals(name))
                            SESmartEconomy.Log.Debug(item.StoreItemId);
                            if (item.StoreItemId == id)
                            {

                                item.BoughtSinceLastUpdate += amount;                               
                               if (SESmartEconomy.Instance.Config.Statistics)
                                   SESmartEconomy.LogStatistic.Info(",GUID:," + __instance.CustomData + ",Bought,Amount:," + amount + ",Item:," + name + ",Price:," + price + ",Player:," + MyEventContext.Current.Sender);
                               SESmartEconomy.Log.Debug("id: " + id + ", GUID: " + __instance.CustomData + ", Bought, Amount: " + amount + ", Item: " + name + ", Price: " + price + ", Player: " + MyEventContext.Current.Sender);

                                OnSendBuytemResult?.Invoke(__instance, id, name, price, amount, result, targetEndpoint);
                                return;
                            }
                        }
                    }
                }
            }
        }

        //avoid impact on grid due to store inventory
        public static bool RefreshVolumeAndMassPatched(MyInventory __instance)
        {
            
            if (!SESmartEconomy.Instance.Config.Enabled)
                return true;
            if (__instance.Entity is MyStoreBlock)
            {
                SESmartEconomy.Log.Trace("RefreshVolumeAndMassPatched");
                // its one of our custom stores
                //SESmartEconomy.Log.Info("refresh");
                SESmartEconomyStore store = GetSESEStore(((MyStoreBlock)__instance.Entity).CustomData);
                if (store != null)
                {
                    //SESmartEconomy.Log.Info("MyStoreBlock RefreshVolumeAndMassPatched abort!");

                    if (Field_m_currentMass == null)
                        SESmartEconomy.Log.Error("Field_m_currentMass is null");
                    if (Field_m_currentVolume == null)
                        SESmartEconomy.Log.Error("Field_m_currentVolume is null");

                    Sync<MyFixedPoint, SyncDirection.FromServer> m_currentMass = (Sync<MyFixedPoint, SyncDirection.FromServer>)Field_m_currentMass.GetValue(__instance);
                    m_currentMass.Value = MyFixedPoint.Zero;
                    Field_m_currentMass.SetValue(__instance, m_currentMass);
                    Sync<MyFixedPoint, SyncDirection.FromServer> m_currentVolume = (Sync<MyFixedPoint, SyncDirection.FromServer>)Field_m_currentVolume.GetValue(__instance);
                    m_currentVolume.Value = MyFixedPoint.Zero;
                    Field_m_currentVolume.SetValue(__instance, m_currentVolume);

                    return false;
                }
            }
            return true;
        }

        public static SESmartEconomyStore GetSESEStore(string customData)
        {
            SESmartEconomy.Log.Trace("GetSESEStore: " + customData);
            foreach (SESmartEconomyStore store in SESmartEconomy.Instance.Config.Stores)
            {
                if (customData.Contains(store.GUID))
                {
                    //SESmartEconomy.Log.Warn(customData + " -- " + store.GUID);
                    //foreach (SESmartEconomyStoreItem storeItem in store.SESEStoreItems)
                    //{
                    //    SESmartEconomy.Log.Warn(storeItem.BoughtSinceLastUpdate + " # " + storeItem.SoldSinceLastUpdate);                       
                    //}
                    return store;
                }
            }
            return null;
        }

        public static bool GetStoreItemsPatched(MyStoreBlock __instance)
        {

            SESmartEconomy.Log.Trace("GetStoreItemsPatched! ");
            if (!SESmartEconomy.Instance.Config.Enabled)
                return true;

                if (!(__instance is MyStoreBlock))
                return true;

            SESmartEconomyStore store = GetSESEStore(__instance.CustomData);
            if (store == null)
            {
                //SESmartEconomy.Log.Info("MyStoreBlock GetStoreItemsPatch abort!");
                return true;
            }

            // check user permissions  
            MyPlayer myPlayer = Sync.Players.GetOnlinePlayers().ToList().Find((MyPlayer x) => x.Id.SteamId == MyEventContext.Current.Sender.Value);
            if (myPlayer != null)
            {
                if (store.AllowedUser.Count == 0 && store.AllowedFactions.Count == 0)
                    return true;

                IMyFaction faction = MySession.Static.Factions.TryGetPlayerFaction(myPlayer.Identity.IdentityId);
                if (faction == null && !store.AllowedUser.Contains(MyEventContext.Current.Sender.Value))
                {
                    return false;
                }
                if (!store.AllowedUser.Contains(MyEventContext.Current.Sender.Value) && !store.AllowedFactions.Contains(faction.FactionId))
                {
                    return false;
                }
            }
            return true;
        }

        public class SESEStoreTimer : System.Timers.Timer
        {
            public MyStoreBlock Block { get; set; }
        }


        /* public static void InitPatched(MyStoreBlock __instance)
         {

             SESmartEconomy.Log.Debug("MyStoreBlock InitPatch! ");
             if (!SESmartEconomy.Instance.Config.Enabled)
                 return;
             if (!(__instance is MyStoreBlock))
                 return;

             SESmartEconomyStore store = GetSESEStore(__instance.CustomData);
             if (store == null)
                 return;

             __instance.OnClosing += Closing;

         }
         */

        private static void OnTimedEvent(Object source, ElapsedEventArgs e)
        {

            SESmartEconomy.Log.Trace("MyStoreBlock  OnTimedEvent");
            if (((SESEStoreTimer)source).Block is MyStoreBlock)
            {
                OnStartWorkingPatched(((SESEStoreTimer)source).Block);
            }
        }

        public static bool OnStartWorkingPatched(MyStoreBlock __instance)
        {
            
            SESmartEconomy.Log.Trace("OnStartWorkingPatched: " + __instance.EntityId);
            if (!SESmartEconomy.Instance.Config.Enabled)
                return true;
            if (!(__instance is MyStoreBlock))
                return true;
            SESmartEconomyStore store = GetSESEStore(__instance.CustomData);
            if (store == null)
            {
                return true;
            }
            if (!startWorkingBlacklistDict.ContainsKey(__instance.EntityId))
            {
                startWorkingBlacklistDict.Add(__instance.EntityId, MySandboxGame.Static.SimulationFrameCounter);
            }
            else
            {
                if (startWorkingBlacklistDict[__instance.EntityId] == MySandboxGame.Static.SimulationFrameCounter)
                {
                    return true;
                }else
                {
                    startWorkingBlacklistDict[__instance.EntityId] = MySandboxGame.Static.SimulationFrameCounter;
                }
            }

                SESmartEconomy.Log.Debug("OnStartWorkingPatched: SESE Store, Frame: " + MySandboxGame.Static.SimulationFrameCounter);
            if (!timerDict.ContainsKey(__instance.EntityId))
            {
                SESEStoreTimer timer = new SESEStoreTimer();
                timerDict.Add(__instance.EntityId, timer);
                timer.Interval = 1000 * store.RefreshRate;
                timer.Block = __instance;
                timer.Elapsed += OnTimedEvent;
                timer.Start();
                __instance.OnClosing += Closing;
            }
            
            if (!storeItemsDict.ContainsKey(store.GUID))
            {
                SESmartEconomy.Log.Debug("Creating Store inventory list: " + store.GUID);
                var list = new List<MyStoreItem>();
                foreach (SESmartEconomyStoreItem storeItem in store.SESEStoreItems)
                {
                    MyStoreItem myStoreItem = new MyStoreItem(MyEntityIdentifier.AllocateId(MyEntityIdentifier.ID_OBJECT_TYPE.STORE_ITEM, MyEntityIdentifier.ID_ALLOCATION_METHOD.RANDOM)
                        , storeItem.ItemDefinitionId, storeItem.Amount, storeItem.BasePrice, storeItem.ListingType);
                    list.Add(myStoreItem);
                    storeItem.StoreItemId = myStoreItem.Id;
                }
                storeItemsDict.Add(store.GUID, list);
            }
            

            // Make the Store awesome
            if (Field_m_maxVolume == null)
                SESmartEconomy.Log.Error("Field_m_maxVolume is null");

            Field_m_maxVolume.SetValue(__instance.GetInventory(), MyFixedPoint.MaxValue);
            //SESmartEconomy.Log.Info("MyStoreBlock GetStoreItemsPatch! " + MyEventContext.Current.Sender);
            //SESmartEconomy.Log.Info("Storeitemscount: " + store.SESEStoreItems.Count);

            // Prepare Store   
            __instance.PlayerItems.Clear();
            __instance.GetInventory().Clear(true);
            Random rnd = new Random();
            foreach (SESmartEconomyStoreItem storeItem in store.SESEStoreItems)
            {
                if (rnd.Next(0, 100) < storeItem.DisplayChance)
                {
                    // calculate dynamic prices
                    if (store.SmartPriceEnabled)
                    {
                        SESmartEconomy.Log.Debug(" ");
                        SESmartEconomy.Log.Debug(storeItem.ItemDefinitionId.ToString());
                        SESmartEconomy.Log.Debug("before storeItem.MarketMultiplier: " + storeItem.MarketMultiplier);
                        SESmartEconomy.Log.Debug("storeItem.Amount: " + storeItem.Amount + ", storeItem.BoughtSinceLastUpdate: " + storeItem.BoughtSinceLastUpdate + ", SoldSinceLastUpdate: " + storeItem.SoldSinceLastUpdate);
                        if (storeItem.ListingType == StoreItemTypes.Offer)
                        {
                            float stock = 1f / storeItem.Amount * storeItem.BoughtSinceLastUpdate;
                            SESmartEconomy.Log.Debug("Stock Offer: " + stock + ", > 0.75 : " + (stock > 0.75f) + ", == 0f : "+ (stock == 0f));
                            if (stock > 0.75f)
                            {
                                SESmartEconomy.Log.Debug("Offer >0.75 triggered");
                                storeItem.MarketMultiplier *= (storeItem.MarketMultiplierListingFillStep + 1);                                
                            }
                            else if (stock == 0f)
                            {
                                SESmartEconomy.Log.Debug("Offer == 0f triggered");
                                storeItem.MarketMultiplier /= (storeItem.MarketMultiplierListingIgnoreStep + 1);                                
                            }
                        }
                        else if (storeItem.ListingType == StoreItemTypes.Order)
                        {
                            float stock = 1f / storeItem.Amount * storeItem.SoldSinceLastUpdate;
                            SESmartEconomy.Log.Debug("Stock order: " + stock + ", > 0.75 : " + (stock > 0.75f) + ", == 0f : " + (stock == 0f));
                            if (stock > 0.75f)
                            {
                                SESmartEconomy.Log.Debug("Order >0.75 triggered");
                                storeItem.MarketMultiplier /= (storeItem.MarketMultiplierListingFillStep + 1);
                            }
                            else if (stock == 0f)
                            {
                                SESmartEconomy.Log.Debug("Order == 0f triggered");
                                storeItem.MarketMultiplier *= (storeItem.MarketMultiplierListingIgnoreStep + 1);
                            }
                        }
                        if (storeItem.MarketMultiplier < 0)
                            storeItem.MarketMultiplier = 0;
                        if (storeItem.MarketMultiplier > storeItem.MarketMultiplierUpperLimit)
                            storeItem.MarketMultiplier = storeItem.MarketMultiplierUpperLimit;
                        if (storeItem.MarketMultiplier < storeItem.MarketMultiplierLowerLimit)
                            storeItem.MarketMultiplier = storeItem.MarketMultiplierLowerLimit;

                        storeItem.BoughtSinceLastUpdate = 0;
                        storeItem.SoldSinceLastUpdate = 0;
                        SESmartEconomy.Log.Debug("after storeItem.MarketMultiplier: " + storeItem.MarketMultiplier);
                    }

                    int price = (int)(storeItem.BasePrice * storeItem.MarketMultiplier);
                    SESmartEconomy.Log.Debug("price: " + price);
                    if (price <= 0)
                        price = 1;
                    //SESmartEconomy.Log.Info("price = " + price + " - " + storeItem.MarketMultiplier);
                    //SESmartEconomy.Log.Info("ignore: " + storeItem.MarketMultiplierListingIgnoreStep + " fill: " + storeItem.MarketMultiplierListingFillStep);

                    //MyStoreItem myStoreItem = new MyStoreItem(MyEntityIdentifier.AllocateId(MyEntityIdentifier.ID_OBJECT_TYPE.STORE_ITEM, MyEntityIdentifier.ID_ALLOCATION_METHOD.RANDOM)
                    //   , storeItem.ItemDefinitionId, storeItem.Amount, price, storeItem.ListingType);
                    MyStoreItem myStoreItem = storeItemsDict[store.GUID].First((item) =>                    
                        item.Id == storeItem.StoreItemId
                    );
                    myStoreItem.PricePerUnit = price;
                    myStoreItem.Amount = storeItem.Amount;
                    __instance.PlayerItems.Add(myStoreItem);

                    if (storeItem.ListingType == StoreItemTypes.Offer)
                    {
                        try
                        {                            
                            MyObjectBuilder_Base myObjectBuilder_Base = MyObjectBuilderSerializer.CreateNewObject(storeItem.ItemDefinitionId);
                            __instance.GetInventory().AddItems(storeItem.Amount, myObjectBuilder_Base);
                        } catch( ArgumentNullException )
                        {
                            SESmartEconomy.Log.Error("ItemDefinitionId is null! \n " + storeItem.ItemDefinitionId.TypeIdString + "\n GUID: " + store.GUID);
                        }
                       
                    }
                }

            }
            __instance.GetInventory().Refresh();
            return true;
        }


        public static void Patch(PatchContext ctx)
        {

            //ctx.GetPattern(Method_Init).Suffixes.Add(Method_InitPatched);

            ctx.GetPattern(Method_OnStartWorking).Prefixes.Add(Method_OnStartWorkingPatched);
            ctx.GetPattern(Method_GetStoreItems).Prefixes.Add(Method_GetStoreItemsPatched);
            ctx.GetPattern(Method_RefreshVolumeAndMass).Prefixes.Add(Method_RefreshVolumeAndMassPatched);
            ctx.GetPattern(Method_SendSellItemResult).Prefixes.Add(Method_SendSellItemResultPatched);
            ctx.GetPattern(Method_SendBuyItemResult).Prefixes.Add(Method_SendBuyItemResultPatched);
            ctx.GetPattern(Method_OnRemovedByCubeBuilder).Prefixes.Add(Method_OnRemovedByCubeBuilderPatched);
            ctx.GetPattern(Method_OnItemBought).Prefixes.Add(Method_OnItemBoughtPatched);
           
            SESmartEconomy.Log.Info("Patching Successfull MyStoreBlock!");

        }
    }
}
