﻿using NLog;
using NLog.Config;
using NLog.Targets;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Torch;
using Torch.API;
using Torch.API.Managers;
using Torch.API.Plugins;
using Torch.API.Session;
using Torch.Managers.PatchManager;
using Torch.Session;
using Torch.Views;
using VRage.Game;

namespace SESmartEconomy
{
    public class SESmartEconomy : TorchPluginBase, IWpfPlugin
    {
        public static readonly Logger Log = LogManager.GetCurrentClassLogger();
        public static Logger LogStatistic;
        private Persistent<SESmartEconomyConfig> _config;
        public SESmartEconomyConfig Config => _config?.Data;
        private UserControl _control;
        public UserControl GetControl() => _control ?? (_control = new PropertyGrid() {
            DataContext = Config,    
        });
        public static SESmartEconomy Instance { get; private set; }
        public IMultiplayerManagerBase multiplayerManagerBase;
        public ITorchSessionManager torchSessionManager;


        public override void Init(ITorchBase torch)
        {
            base.Init(torch);
            Instance = this;
            SetupConfig();
            if (!Config.Enabled)
                return;
            if (Config.Stores.Count == 0)
            {
                Log.Warn("No Stores found in config! Please make sure you set up your config file! Check the plugins documentary for a sample file!");
            }

            torchSessionManager = Torch.Managers.GetManager<TorchSessionManager>();
            if (torchSessionManager != null)
                torchSessionManager.SessionStateChanged += SessionChanged;
            else
                Log.Warn("No session manager loaded!");
            
            Directory.CreateDirectory(Path.GetDirectoryName(Config.StatisticsLog));


            var statTarget = new FileTarget
            {
                FileName = "${basedir}/" + Config.StatisticsLog,
                Layout = "${longdate} - ${message} ${exception:format=tostring}"
            };
            LogManager.Configuration.AddTarget("statistics", statTarget);
            LoggingRule rule = new LoggingRule("SESEStats", LogLevel.Info, statTarget)
            {
                Final = true
            };
            LogManager.Configuration.LoggingRules.Insert(0, rule);
            LogManager.Configuration.Reload();
            LogStatistic = LogManager.GetLogger("SESEStats");

            LogStatistic.Info("Plugin started");
            Log.Info("SESmartEconomy initialized!");


        }
        private void SessionChanged(ITorchSession session, TorchSessionState state)
        {
            switch (state)
            {
                case TorchSessionState.Loaded:
                    multiplayerManagerBase = Torch.CurrentSession.Managers.GetManager<IMultiplayerManagerBase>();
                    if (multiplayerManagerBase != null)
                    {
                       
                        //var x = new SESmartEconomyStore("asd");
                        //var y = new SESmartEconomyStoreItem(new MyDefinitionId(typeof(MyObjectBuilder_Component), "BulletproofGlass"),1337,7331);
                        //x.SESEStoreItems.Add(y);
                        //Config.Stores.Add(x);
                        //SetupConfig();                        
                        foreach(var store in Config.Stores)
                        {
                            store.PrepareItems();
                        }

                    }
                    else
                    {
                        Log.Error("MultiplayerManagerBase is null");
                    }
                    break;
                case TorchSessionState.Unloading:
                    if (multiplayerManagerBase != null)
                    {
                        Save();
                    }
                    break;
            }
        }


        public void SetupConfig()
        {
            var configFile = Path.Combine(StoragePath, "SESmartEconomy.cfg");
            try
            {
                _config = Persistent<SESmartEconomyConfig>.Load(configFile);
                if (multiplayerManagerBase != null)
                {
                    foreach (var store in _config?.Data?.Stores)
                    {
                        store.PrepareItems();
                    }
                }

            }
            catch (Exception e)
            {
                Log.Warn(e);
            }

            if (_config?.Data == null)
            {
                Log.Info("Create Default Config, because none was found!");
                _config = new Persistent<SESmartEconomyConfig>(configFile, new SESmartEconomyConfig());
                Save();
            }
        }

        public void Save()
        {
            try
            {
                
                _config.Save();
                Log.Info("Configuration Saved.");
                
            }
            catch (IOException e)
            {
                Log.Warn(e, "Configuration failed to save");
            }
        }
    }
}
