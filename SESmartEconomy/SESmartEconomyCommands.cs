﻿using Sandbox.Definitions;
using Sandbox.Game.Multiplayer;
using Sandbox.Game.World;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Torch.Commands;
using Torch.Commands.Permissions;
using VRage;
using VRage.Game.ModAPI;

namespace SESmartEconomy
{
    [Category("SESE")]
    public class SESmartEconomyCommands : CommandModule
    {

        private FastResourceLock configLock = new FastResourceLock();

        [Command("AllowStore", "Adds a player to a Store type")]
        [Permission(MyPromoteLevel.Moderator)]
        public void AllowStore(ulong steamID, string guid)
        {
            SESmartEconomyStore store = MyStoreBlockPatch.GetSESEStore(guid);
            if (store == null)
            {
                Context.Respond("Error, Store not found!", "SESE");
                return;
            }
            MyPlayer myPlayer = Sync.Players.GetOnlinePlayers().ToList().Find((MyPlayer x) => x.Id.SteamId == steamID);
            if (myPlayer == null)
            {
                Context.Respond("Error, No player with that steamID64 found!", "SESE");
                return;
            }
            store.AllowedUser.Add(steamID);
            SESmartEconomy.Instance.Save();
            Context.Respond("User was added to Stores of type: " + guid, "SESE", "Green");
        }
        [Command("DenyStore", "Removes a player from a Store type")]
        [Permission(MyPromoteLevel.Moderator)]
        public void DenieStore(ulong steamID, string guid)
        {
            SESmartEconomyStore store = MyStoreBlockPatch.GetSESEStore(guid);
            if (store == null)
            {
                Context.Respond("Error, Store not found!", "SESE");
                return;
            }
            MyPlayer myPlayer = Sync.Players.GetOnlinePlayers().ToList().Find((MyPlayer x) => x.Id.SteamId == steamID);
            if (myPlayer == null)
            {
                Context.Respond("Error, No player with that steamID64 found!", "SESE");
                return;
            }
            store.AllowedUser.Remove(steamID);
            SESmartEconomy.Instance.Save();
            Context.Respond("User was removed from Stores of type: " + guid, "SESE", "Green");
        }

        [Command("AllowStoreFaction", "Adds a faction to a Store type")]
        [Permission(MyPromoteLevel.Moderator)]
        public void AllowStoreFaction(string tag, string guid)
        {
            SESmartEconomyStore store = MyStoreBlockPatch.GetSESEStore(guid);
            if (store == null)
            {
                Context.Respond("Error, Store not found!", "SESE");
                return;
            }
            IMyFaction faction = MySession.Static.Factions.TryGetFactionByTag(tag);
            if (faction == null)
            {
                Context.Respond("Error, No faction with that tag found!", "SESE");
                return;
            }
            store.AllowedFactions.Add(faction.FactionId);
            SESmartEconomy.Instance.Save();
            Context.Respond("Faction was added to Stores of type: " + guid, "SESE", "Green");
        }

        [Command("DenyStoreFaction", "Removes a faction from a Store type")]
        [Permission(MyPromoteLevel.Moderator)]
        public void DenieStoreFaction(string tag, string guid)
        {
            SESmartEconomyStore store = MyStoreBlockPatch.GetSESEStore(guid);
            if (store == null)
            {
                Context.Respond("Error, Store not found!", "SESE");
                return;
            }
            IMyFaction faction = MySession.Static.Factions.TryGetFactionByTag(tag);
            if (faction == null)
            {
                Context.Respond("Error, No faction with that tag found!", "SESE");
                return;
            }
            store.AllowedFactions.Remove(faction.FactionId);
            SESmartEconomy.Instance.Save();
            Context.Respond("Faction was removed from Stores of type: " + guid, "SESE", "Green");
        }

        [Command("Reload", "Reloads the Plugin Config")]
        [Permission(MyPromoteLevel.Moderator)]
        public void Reload()
        {
            
            SESmartEconomy.Instance.SetupConfig();
            SESmartEconomy.Log.Debug("config reloaded");

            MyStoreBlockPatch.ReloadStores();
            MyStoreBlockPatch.ReloadTimer();             
            SESmartEconomy.Log.Debug("timer done");
            Context.Respond("Config reloaded, Stores updated!","SESE","Green");
            
        }

        
        [Command("RefreshStores", "Performs a full refresh cycle on all stores")]
        [Permission(MyPromoteLevel.Moderator)]
        public void RefreshStores()
        {
            MyStoreBlockPatch.RefreshStores();
            Context.Respond("Refresh done!", "SESE", "Green");
        }
        

        [Command("ShowStores", "Displays all Storetypes")]
        [Permission(MyPromoteLevel.Moderator)]
        public void ShowStores()
        {
            Context.Respond("Showing " + SESmartEconomy.Instance.Config.Stores.Count + " Stores:", "SESE", "Green");
            foreach (SESmartEconomyStore store in SESmartEconomy.Instance.Config.Stores)
            {
                Context.Respond("GUID: " + store.GUID, "SESE", "Green");
            }
 
        }

        [Command("DumpDefinitions", "Dumps all available items")]
        [Permission(MyPromoteLevel.Moderator)]
        public void DumpDefinitions()
        {
            Context.Respond("Dumping definitions to file SESEDefinitions.txt");
            var file = File.CreateText("SESEDefinitions.txt");
            String dump = "";

            foreach (var def in MyDefinitionManager.Static.Definitions.Definitions)
            {
                foreach(var idk in def.Value)
                {
                    dump += "<ItemDefinitionType>" + idk.Value.Id.TypeId.ToString() + "</ItemDefinitionType>\n"
                        + "<ItemDefinitionSubType>" + idk.Value.Id.SubtypeName + "</ItemDefinitionSubType>\n\n";
                    
                }
            }
            file.WriteLine(dump);
            file.Close();
        }

    }
}
