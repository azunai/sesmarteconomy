﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Torch;
using Torch.Views;

namespace SESmartEconomy
{
    public class SESmartEconomyConfig : ViewModel
    {
        private HashSet<SESmartEconomyStore> _Stores = new HashSet<SESmartEconomyStore>();
        private bool _Enabled = false;
        private bool _Statistics = false;
        private string _Info = "https://bitbucket.org/azunai/sesmarteconomy";
        private string _StatisticsLog = "Logs/SESEStatistics.log";

        [Display(Visible = false)]
        public HashSet<SESmartEconomyStore> Stores { get => _Stores; set => SetValue(ref _Stores, value); }
        //public HashSet<SESmartEconomyStore> Stores { get => _Stores; set => _Stores = value; }

        [Display(Enabled = true, Name = "Plugin Enabled:", GroupName = "Settings", Description = "Enable or Disable the plugin. Requiers restart to take affect!")]
        public bool Enabled { get => _Enabled; set => SetValue(ref _Enabled, value); }
        //public bool Enabled { get => _Enabled; set => _Enabled = value; }

        [Display(Enabled = true, Name = "Enable Statistics:", GroupName = "Settings", Description = "Creates a Logfile in your Instance folder logging all Sells and Buys. The log can be imported to Excel with commata as delimiter")]
        public bool Statistics { get => _Statistics; set => SetValue(ref _Statistics, value); }
        //public bool Statistics { get => _Statistics; set => _Statistics = value; }

        [Display(Enabled = false, Name = "Repository:", GroupName = "Documentation and bug reports")]
        public string Info { get => _Info; }

        [Display(Enabled = true, Name = "Statistics Logfile:", GroupName = "Settings", Description = "Location of the logfile, requiers a restart")]
        public string StatisticsLog { get => _StatisticsLog; set => SetValue(ref _StatisticsLog, value); }
        //public string StatisticsLog  { get => _StatisticsLog; set => _StatisticsLog = value; }
    }
}

