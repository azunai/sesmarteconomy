﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using VRage.Game;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.ObjectBuilders;

namespace SESmartEconomy
{
    public class SESmartEconomyStore
    {
        public string GUID { get; set; }
        public uint RefreshRate { get; set; }    
        public bool SmartPriceEnabled { get; set; }
        public List<SESmartEconomyStoreItem> SESEStoreItems { get; set; } 
        public HashSet<ulong> AllowedUser { get; set; }
        public HashSet<long> AllowedFactions { get; set; }

        public SESmartEconomyStore(string guid)
        {
            GUID = guid;
            SESEStoreItems = new List<SESmartEconomyStoreItem>();
            AllowedUser = new HashSet<ulong>();
            AllowedFactions = new HashSet<long>();
            RefreshRate = 60;
        }
        public SESmartEconomyStore(){ }

        public void PrepareItems()
        {
            foreach (var item in SESEStoreItems)
            {
                item.Prepare();
            }
        }

    }

    public class SESmartEconomyStoreItem
    {
        [XmlIgnore]
        public long StoreItemId { get; set; }
        public String ItemDefinitionType { get; set; }
        public String ItemDefinitionSubType { get; set; }
        [XmlIgnore]
        public SerializableDefinitionId ItemDefinitionId { get; set; }
        public int Amount { get; set; }
        public int BasePrice { get; set; }
        public StoreItemTypes ListingType { get; set; }
        public float DisplayChance { get; set; }
        public float MarketMultiplier { get; set; }
        public float MarketMultiplierListingFillStep { get; set; }
        public float MarketMultiplierListingIgnoreStep { get; set; }
        public float MarketMultiplierUpperLimit { get; set; }
        public float MarketMultiplierLowerLimit { get; set; }
        public int SoldSinceLastUpdate { get; set; }
        public int BoughtSinceLastUpdate { get; set; }
        public SESmartEconomyStoreItem(SerializableDefinitionId itemDefinitionId, int amount, int basePrice, StoreItemTypes listingType, float displayChance, float marketMultiplier)
        {
            ItemDefinitionId = itemDefinitionId;
            Amount = amount;
            BasePrice = basePrice;
            ListingType = listingType;
            DisplayChance = displayChance;
            MarketMultiplier = marketMultiplier;
            SoldSinceLastUpdate = 0;
            BoughtSinceLastUpdate = 0;
            MarketMultiplierUpperLimit = 2;
            MarketMultiplierLowerLimit = 0;
            MarketMultiplierListingIgnoreStep = 0.025f;
            MarketMultiplierListingFillStep = 0.1f;

        }
        public void Prepare()
        {
            SESmartEconomy.Log.Debug("trying to load: " + ItemDefinitionType + " | " + ItemDefinitionSubType);
            ItemDefinitionId = new SerializableDefinitionId(MyObjectBuilderType.Parse(ItemDefinitionType), ItemDefinitionSubType);
        }
        public SESmartEconomyStoreItem() { }

    }
}
